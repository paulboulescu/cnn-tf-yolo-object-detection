
import tensorflow as tf
from utils import *


class InputLayer:

    def __init__(self, name, role):
        self.resources = {}
        self.resources['name'] = name
        self.resources['type'] = role
        self.output = None

    def run(self, x):
        self.output = x


class ConvolutionalLayer:

    def __init__(self, name, role, batch_normalize, channels, filter_size, stride, padding, activation, in_channels):
        self.resources = {}
        self.resources['name'] = name
        self.resources['type'] = role
        self.resources['batch_norm'] = batch_normalize
        self.resources['channels'] = channels
        self.resources['filter_size'] = filter_size
        self.resources['stride'] = stride
        self.resources['padding'] = padding
        self.resources['activation'] = activation
        self.output = None

    def run(self, x, weights, counter, training=False):

        # Convolution
        z = tf.nn.conv2d(x, weights, strides=[1, self.resources['stride'],
                                              self.resources['stride'], 1],
                         padding=str(self.resources['padding']),
                         name='conv_' + self.resources['name'])

        # Normalization
        if self.resources['batch_norm']:
            z = tf.layers.batch_normalization(z,
                                              training=training,
                                              scale=False,
                                              name='batch_norm_' + self.resources['name'],
                                              reuse=tf.AUTO_REUSE)

        # Activation
        if self.resources['activation'] == "leaky":
            self.output = tf.nn.leaky_relu(z, alpha=0.1, name='leaky_relu_'+str(counter))
        else:
            self.output = z


class UpsampleLayer:

    def __init__(self, name, role, stride):
        self.resources = {}
        self.resources['name'] = name
        self.resources['type'] = role
        self.resources['stride'] = stride
        self.output = None

    def run(self, x):
        new_height = tf.shape(x)[1] * self.resources['stride']
        new_width = tf.shape(x)[2] * self.resources['stride']
        self.output = tf.image.resize_images(x, [new_height, new_width])


class RouteLayer:

    def __init__(self, name, role, layers):
        self.resources = {}
        self.resources['name'] = name
        self.resources['type'] = role
        self.resources['layers'] = layers
        self.output = None

    def run(self, feature_maps):
        self.output = tf.concat(feature_maps, 3, name='concat_'+self.resources['name'])


class ShortcuyLayer:

    def __init__(self, name, role, reference, activation):
        self.resources = {}
        self.resources['name'] = name
        self.resources['type'] = role
        self.resources['shortcut'] = reference
        self.resources['activation'] = activation
        self.output = None

    def run(self, feature_maps):
        self.output = tf.add_n(feature_maps, name='shortcut_'+self.resources['name'])


class YOLOLayer:

    def __init__(self, name, role, scale, num, jitter, ignore_thresh, truth_thresh, random):
        self.resources = {}
        self.resources['name'] = name
        self.resources['type'] = role
        self.resources['scale'] = scale
        self.resources['num'] = num
        self.resources['jitter'] = jitter
        self.resources['ignore_thresh'] = ignore_thresh
        self.resources['truth_thresh'] = truth_thresh
        self.resources['random'] = random
        self.output = None

    def run(self, z, counter, training=False):

        # Predict_transforms Ys from (m, h, w, c) into (m, h*w, c)
        # Each row will contain [tx, ty, tw, th, p0, c1, c2, ...]
        z = yolo_transform(z, counter)

        if training is False:
            width = tf.shape(z)[2]
            t_coords_pred = tf.slice(z, [0, 0, 0], [-1, -1, 2])
            t_size_pred = tf.slice(z, [0, 0, 2], [-1, -1, 2])
            p_pred = tf.slice(z, [0, 0, 4], [-1, -1, 1])
            c_pred = tf.slice(z, [0, 0, 5], [-1, -1, width - 5])

            self.output = tf.concat([t_coords_pred, t_size_pred, tf.sigmoid(p_pred), tf.sigmoid(c_pred)],-1)

        else:
            self.output = z

        return self.output
