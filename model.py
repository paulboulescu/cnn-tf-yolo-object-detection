
from layers import *
from data_reader import *
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
from utils import *
from scipy.special import expit
from prediction import *


def create_placeholders():

    """
    Creates the placeholders for the tensorflow session

    Returns:
    x -- placeholder for input data
    y -- placeholder for labels data
    """

    depth = (np.shape(CONFIG.CLASSES)[0] + 5)

    # Will hold the input data - shape [batch, height, width, channels]
    x = tf.placeholder(tf.float32, [None, CONFIG.HEIGHT, CONFIG.WIDTH, CONFIG.CHANNELS])

    # Will hold the labels data
    y = tf.placeholder(tf.float32, [None, np.square(CONFIG.DETECTION_RESOLUTION[0]) * np.shape(CONFIG.ANCHORS)[0] +
                                      np.square(CONFIG.DETECTION_RESOLUTION[1]) * np.shape(CONFIG.ANCHORS)[0] +
                                      np.square(CONFIG.DETECTION_RESOLUTION[2]) * np.shape(CONFIG.ANCHORS)[0],
                                      depth])

    return x, y


def initialize_parameters(layers_data, shapes):

    """
    Initializes weight parameters used in the Convolutional Layers of the network

    Arguments:
    layers_data -- information about the network, loaded from the configuration file
    shapes -- shapes of different layers across the network

    Returns:
    parameters -- generated weights
    """

    parameters = {}
    parameters_counter = 0

    for layer_index, layer_data in enumerate(layers_data):
        if layer_data['type'] == 'convolutional':
            parameters["w_"+str(parameters_counter)] = \
                tf.get_variable(
                    "w_"+str(layer_index),
                    [layer_data['size'], layer_data['size'], shapes[layer_index - 1][2], layer_data['filters']],
                    initializer = tf.contrib.layers.xavier_initializer())
            parameters_counter += 1

    return parameters


def create_network(layers_data, shapes):

    """
    Creates the network based on information loaded from the configuration file

    Arguments:
    layers_data -- information about the network, loaded from the configuration file
    shapes -- shapes of different layers across the network

    Returns:
    layers -- list of all the layers in the network
    """

    layers = []

    for layer_index, layer_data in enumerate(layers_data):
        if layer_data['type'] == 'input':
            layer = InputLayer('layer_' + str(layer_index), 'input')

        elif layer_data['type'] == 'convolutional':
            layer = ConvolutionalLayer('layer_' + str(layer_index), 'convolutional', layer_data['batch_normalize'],
                                        layer_data['filters'], layer_data['size'], layer_data['stride'],
                                        layer_data['pad'], layer_data['activation'], shapes[layer_index-1][2])

        elif layer_data['type'] == 'upsample':
            layer = UpsampleLayer('layer_' + str(layer_index), 'upsample', layer_data['stride'])

        elif layer_data['type'] == 'route':
            layer = RouteLayer('layer_' + str(layer_index), 'route', layer_data['layers'])

        elif layer_data['type'] == 'shortcut':
            layer = ShortcuyLayer('layer_' + str(layer_index), 'shortcut', layer_data['from'],
                                  layer_data['activation'])

        elif layer_data['type'] == 'yolo':
            layer = YOLOLayer('layer_' + str(layer_index), 'yolo', layer_data['scale'], layer_data['num'],
                              layer_data['jitter'], layer_data['ignore_thresh'], layer_data['truth_thresh'],
                              layer_data['random'])

        layers.append(layer)

    return layers


def forward_propagation(layers, x, parameters, training=False):

    """

    Performs Forward propagation over the network

    Arguments:
    layers -- the network, structured as a list of different layers
    x -- input data
    parameters -- weights used in the convolutional layer
    training -- boolean, if this forward propagation is used while training or not

    Returns:
    y_hat -- the output of the forward propagation

    """

    # Will store y_hat values for each of the scales
    y_hat = []

    conv_counter = 0
    yolo_counter = 0

    for layer_index, layer in enumerate(layers):

        if layer.resources['type'] == 'input':
            layer.run(x)

        elif layer.resources['type'] == 'convolutional':
            layer.run(layers[layer_index-1].output, parameters['w_'+str(conv_counter)], conv_counter, training)
            conv_counter += 1

        elif layer.resources['type'] == 'upsample':
            layer.run(layers[layer_index - 1].output)

        elif layer.resources['type'] == 'route':
            feature_maps = []
            for route_index, route_layer_index in enumerate(layer.resources['layers']):
                feature_maps.append(layers[route_layer_index].output)

            layer.run(feature_maps)

        elif layer.resources['type'] == 'shortcut':
            # Layers referenced for shortcut are added to a list, and will be summed later
            feature_maps = []
            feature_maps.append(layers[layer_index - 1].output)
            feature_maps.append(layers[layer.resources['shortcut']].output)
            layer.run(feature_maps)

        elif layer.resources['type'] == 'yolo':
            y_hat.append(tf.convert_to_tensor(layer.run(layers[layer_index - 1].output, yolo_counter, training)))
            yolo_counter += 1

    y_hat = tf.concat(y_hat, axis=1)

    return y_hat


def compute_cost(y_hat, y):

    """
    Calculates cost of the network for the current set of weights

    Arguments:
    y_hat -- the output of the network, for the current input data
    y -- the labels of the network, for the current input data

    Returns:
    cost -- cost of the network

    """

    y_label = y
    y_pred = y_hat

    width = tf.shape(y_label)[2]

    # data is split in box coordinates, box sizes, objectness probabilities and classes
    # for each of these, different costs will be applied
    t_coords_labels = tf.slice(y_label, [0, 0, 0], [-1, -1, 2])
    t_sizes_labels = tf.slice(y_label, [0, 0, 2], [-1, -1, 2])
    p_labels = tf.slice(y_label, [0, 0, 4], [-1, -1, 1])
    c_labels = tf.slice(y_label, [0, 0, 5], [-1, -1, width - 5])

    t_coords_pred = tf.slice(y_pred, [0, 0, 0], [-1, -1, 2])
    t_sizes_pred = tf.slice(y_pred, [0, 0, 2], [-1, -1, 2])
    p_pred = tf.slice(y_pred, [0, 0, 4], [-1, -1, 1])
    c_pred = tf.slice(y_pred, [0, 0, 5], [-1, -1, width - 5])

    # used to separate labels with objectnesss score =1 from those <threshold
    one = tf.constant(1, dtype=tf.float32)
    zero = tf.constant(0, dtype=tf.float32)

    mask_one = tf.cast(tf.equal(p_labels, one), dtype=tf.float32)
    mask_zero = tf.cast(tf.equal(p_labels, zero), dtype=tf.float32)

    # CASE 1: Pc = 1 -> Cost for everything

    # coordinates
    t_coords_squared_error_loss = tf.multiply(tf.square(tf.subtract(t_coords_labels, t_coords_pred)), mask_one)

    # size
    t_sizes_squared_error_loss = tf.multiply(tf.square(tf.subtract(t_sizes_labels, t_sizes_pred)), mask_one)

    # objectness score
    p_one_cross_entropy_loss = tf.multiply(tf.nn.sigmoid_cross_entropy_with_logits(labels=p_labels,
                                                                                   logits=p_pred), mask_one)

    # class
    c_cross_entropy_loss = tf.multiply(tf.nn.sigmoid_cross_entropy_with_logits(labels=c_labels,
                                                                               logits=c_pred), mask_one)

    # CASE 2: Pc = 0 (Pc < threshold) -> Only objectness cost
    p_zero_cross_entropy_loss = tf.multiply(tf.nn.sigmoid_cross_entropy_with_logits(labels=p_labels,
                                                                                    logits=p_pred), mask_zero)

    # CASE 3: Pc!=0 and Pc!=1 (threshold < Pc <1) -> No cost for this case!

    # costs for features of each image
    t_coords_squared_error_cost = tf.reduce_sum(tf.reduce_sum(t_coords_squared_error_loss, axis=-1), axis=-1)
    t_sizes_squared_error_cost = tf.reduce_sum(tf.reduce_sum(t_sizes_squared_error_loss, axis=-1), axis=-1)
    p_one_cross_entropy_cost = tf.reduce_sum(p_one_cross_entropy_loss, axis=-1)
    c_cross_entropy_cost = tf.reduce_sum(tf.reduce_sum(c_cross_entropy_loss, axis=-1), axis=-1)
    p_zero_cross_entropy_cost = tf.reduce_sum(p_zero_cross_entropy_loss, axis=-1)

    # total cost over dataset
    cost = tf.reduce_mean(t_coords_squared_error_cost + t_sizes_squared_error_cost + p_one_cross_entropy_cost +
                          c_cross_entropy_cost + p_zero_cross_entropy_cost)

    return cost


def model(architecture_data, adapted_annotations, layers_shapes, y_labels, print_cost=True):

    """
    Performs training of the network

    Arguments:
    architecture_data -- information about the architecture of the network, loaded from the configuration file
    adapted_annotations -- information about the training data, used to load images into mini-batches
    layers_shapes -- shapes of different layers within the network
    y_labels -- labels
    print_cost -- boolean, if cost should be printed or not

    Returns:
    parameters -- trained parameteres

    """

    m = len(adapted_annotations)

    # To keep track of the cost
    costs = []

    # Create placeholders
    x, y = create_placeholders()

    # Initialize parameters
    parameters = initialize_parameters(architecture_data, layers_shapes)

    # Create network
    layers = create_network(architecture_data, layers_shapes)

    # Forward propagation
    y_hat = forward_propagation(layers, x, parameters, True)

    # Cost function: Add cost function to tensorflow graph
    cost = compute_cost(y_hat, y)

    # Backpropagation, Adam Optimizer used to minimizes the cost
    optimizer = tf.train.AdamOptimizer(learning_rate=CONFIG.LEARNING_RATE).minimize(cost)

    # Initialize all the variables globally
    init = tf.global_variables_initializer()

    # Start the session to compute the tensorflow graph
    with tf.Session() as sess:

        # Run the initialization (of all the variables)
        sess.run(init)

        # Do the training loop
        for epoch_index in range(CONFIG.NO_EPOCHS):

            # Create a variable to store the total cost
            minibatch_cost = 0.

            # Number of minibatches of size minibatch_size in the train set
            num_minibatches = int(m / CONFIG.MINI_BATCH_SIZE)+1

            # Run through all the minibatches
            for mini_batch_index in range(num_minibatches):

                # Create minibatch
                mb_x, mb_y = get_mini_batches(adapted_annotations, y_labels, mini_batch_index)

                # Run the session to execute the optimizer and the cost, over a minibatch of x and y
                _, temp_cost = sess.run([optimizer, cost], feed_dict={x: mb_x,
                                                                      y: mb_y})

                # Calculate the mean of the costs across all the minibatches
                minibatch_cost += temp_cost / num_minibatches

            # Print the cost every epoch_index
            if print_cost == True and epoch_index % 5 == 0:
                pass
            if print_cost == True and epoch_index % 1 == 0:
                print("Cost after epoch %i: %f" % (epoch_index, minibatch_cost))
                costs.append(minibatch_cost)

        # Plot the cost
        plt.plot(np.squeeze(costs))
        plt.ylabel('cost')
        plt.xlabel('iterations (per tens)')
        plt.title("Learning rate =" + str(CONFIG.LEARNING_RATE))
        plt.show()

        return parameters


# Load the YOLO network architecture from the configuration file
architecture_data = load_architecture_data()

# Parse and adapt the congiguration data
architecture_data, layers_shapes = adapt_architecture_data(architecture_data)

# Determine the paths for annotation files
annotation_files_paths = get_annotations_paths()

# Load annotations data
images_annotations = load_annotations(annotation_files_paths[312:313])

# Adapt annotations data to the network structure
adapted_annotations = adapt_annotations(images_annotations)

# Generate labels
y_labels = generate_labels(adapted_annotations)

# Run the model function
parameters = model(architecture_data, adapted_annotations, layers_shapes, y_labels)


"""
# Display ground truth boxes over an image
batch = get_batch(adapted_annotations)
probs, boxes, classes = get_predictions(y_labels[0])
image = draw_dections(batch[0], probs, boxes, classes)
show_image(image)
"""


# Display box predictions over an image

# Create network
layers = create_network(architecture_data, layers_shapes)
x = get_batch(adapted_annotations[0:1])

# Forward propagation
y_hat = forward_propagation(layers, x, parameters, False)

init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    y_hat = np.array(sess.run(y_hat))

probs, boxes, classes = get_predictions(y_hat[0])

image = draw_dections(x[0], boxes)

show_image(image)
