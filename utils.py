
import numpy as np
import tensorflow as tf
from utils import *


class CONFIG:
    WIDTH = 416
    HEIGHT = 416
    CHANNELS = 3
    DATA_PATH = 'cfg/yolov3.cfg'
    ANNOATIONS_PATH = "data/Annotations"

    # CLASSES = ["aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat", "chair", "cow",
    #                        "diningtable", "dog", "horse", "motorbike", "person", "pottedplant", "sheep", "sofa",
    #                        "train", "tvmonitor"]

    CLASSES = ["aeroplane"]

    IMAGES_PATH = "data/JPEGImages/"

    # Size of a cell, for each resolution
    DETECTION_SIZES = [32, 16, 8]

    # Number of a cells, for each resolution
    DETECTION_RESOLUTION = [13, 26, 52]

    ANCHORS = [[[10, 13], [16, 30], [33, 23]], [[30, 61], [62, 45], [59, 119]], [[116, 90], [156, 198], [373, 326]]]
    LEARNING_RATE = 0.001
    NO_EPOCHS = 100
    MINI_BATCH_SIZE = 16
    MAX_PRED_BOXES = 20
    OBJECTNESS_THRESHOLD = 0.5
    IOU_THRESHOLD = 0.7


def label_transform(input):

    """
    Transforms the labels shape

    Arguments:
    input -- [batch, height, width, (5 + no_of_classes) * no_of anchors]

    Returns:
    parameters -- [batch, height * width * no_of_anchors, (5 + no_of_classes)]
    """

    height_size = np.shape(input)[1]
    width_size = np.shape(input)[2]
    depth_size = np.shape(CONFIG.CLASSES)[0]+5
    output = np.reshape(input, [-1, height_size*width_size*np.shape(CONFIG.ANCHORS)[0], depth_size])

    return output


def yolo_transform(input, counter):

    """
    Transforms the detection surface shape

    Arguments:
    input -- [batch, height, width, (5 + no_of_classes) * no_of anchors]

    Returns:
    parameters -- [batch, height * width * no_of_anchors, (5 + no_of_classes)]
    """

    height_size = tf.shape(input)[1]
    width_size = tf.shape(input)[2]
    depth_size = tf.shape(CONFIG.CLASSES)[0]+5
    output = tf.reshape(input, [-1, height_size*width_size*np.shape(CONFIG.ANCHORS)[0],
                                depth_size], name='reshape_'+str(counter))

    return output


def get_mini_batches(adapted_annotations, y_labels, batch_no):

    """
    Creates mini-batches for training

    Arguments:
    adapted_annotations -- information about the training data, used to load images into mini-batches
    y_labels -- labels of the dataset
    batch_no -- the number the batch within the current epoch, used to determine which images to load

    Returns:
    mini_batch_x -- mini-batch of input data
    mini_batch_y -- mini-batch of labels
    """

    mini_batch_x = []
    m = np.shape(adapted_annotations)[0]
    start_index = batch_no * CONFIG.MINI_BATCH_SIZE

    # The last batch of the epoch might have fewer entries than the rest
    end_index = np.minimum(m, start_index+CONFIG.MINI_BATCH_SIZE)

    for image_index, image_data in enumerate(adapted_annotations[start_index:end_index]):
        image = load_image(image_data['filename'])
        image = resize_image(image, image_data)
        with tf.Session() as sess:
            mini_batch_x.append(sess.run(image))

    mini_batch_y = y_labels[start_index:end_index, :, :]

    return mini_batch_x, mini_batch_y


def get_batch(adapted_annotations):

    """
    Creates a batch for testing

    Arguments:
    adapted_annotations -- information about the training data, used to load images into mini-batches

    Returns:
    mini_batch_x -- batch of input data
    """

    batch_x = []

    for image_index, image_data in enumerate(adapted_annotations):
        image = load_image(image_data['filename'])
        image = resize_image(image, image_data)
        batch_x.append(image)

    return batch_x


def load_image(path):

    """
    Loads image for training of testing

    Arguments:
    path -- name of the file

    Returns:
    image -- image converted into tensor
    """

    image = tf.image.decode_jpeg(tf.read_file(CONFIG.IMAGES_PATH+path), channels=3)
    image = tf.image.convert_image_dtype(image, dtype=tf.float32)
    image = tf.convert_to_tensor(image, dtype=tf.float32)

    return image


def resize_image(image,image_data):

    """
    Resizes image to fit the expected shape, padding is added if necessary

    Arguments:
    image -- name of the file
    image_data -- information about the image, including resize

    Returns:
    image -- resized image
    """

    image = tf.image.resize_images(image, [image_data['size']['resized_height'], image_data['size']['resized_width']])
    image = tf.image.resize_image_with_crop_or_pad(image, CONFIG.HEIGHT, CONFIG.WIDTH)

    return image
