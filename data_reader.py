
import numpy as np
from os import listdir
from os.path import isfile, join
import xml.etree.ElementTree as ET
from utils import *
from scipy.special import logit, expit


def load_architecture_data():

    """

    Loads data regarding the architecture of the network

    Returns:
    layers -- list of information about each layer of the network
    """

    file = open(CONFIG.DATA_PATH, 'r')
    # Store the lines in a list
    lines = file.read().split('\n')
    # Eliminates empty lines
    lines = [x for x in lines if len(x) > 0]
    # Eliminates comments
    lines = [x for x in lines if x[0] != '#']
    # Eliminates whitespaces
    lines = [x.rstrip().lstrip() for x in lines]

    layers = []
    layer = {}

    for line in lines:
        # This marks the start of a new block
        if line[0] == '[':
            # If block is not empty, implies it is storing values of previous block.
            if bool(layer) is True:
                # Add it to the blocks list
                layers.append(layer)
                # Re-init the block
                layer = {}
            layer['type'] = line[1:-1].rstrip()

        else:
            key, value = line.split('=')
            layer[key.rstrip()] = value.lstrip()

    # Adds the last block to the blocks list
    layers.append(layer)

    return layers


def adapt_architecture_data(layers):

    """
    Adapts loaded architecture data in order to ease reading and implementation

    Arguments:
    layers -- information about the network, as loaded from the configuration file

    Returns:
    layers -- adapted information about the network
    shapes -- shapes of each layer in the network

    """

    shapes = []

    for layer_index, layer in enumerate(layers):
        if layer['type'] == 'input':
            shapes.append([CONFIG.WIDTH, CONFIG.HEIGHT, CONFIG.CHANNELS])

        elif layer['type'] == 'convolutional':

            if 'batch_normalize' in layer:
                layer['batch_normalize'] = True if (int(layer['batch_normalize'])) else False

            else:
                layer['batch_normalize'] = False

            layer['filters'] = int(layer['filters'])
            layer['size'] = int(layer['size'])
            layer['stride'] = int(layer['stride'])
            layer['pad'] = 'SAME'   # 'SAME' if (layer['stride'] == 1) else 'VALID'

            if layer['pad'] == 'SAME':
                shapes.append([shapes[layer_index-1][0], shapes[layer_index-1][1], layer['filters']])

            else:
                shapes.append([shapes[layer_index-1][0]/layer['stride'], shapes[layer_index-1][1]/layer['stride'],
                               layer['filters']])

        elif layer['type'] == 'upsample':
            # Upsample performs a blinear deconvolution
            layer['stride'] = int(layer['stride'])
            shapes.append([shapes[layer_index-1][0]*layer['stride'], shapes[layer_index-1][1]*layer['stride'],
                           shapes[layer_index-1][2]])

        elif layer['type'] == 'route':

            # Route concatenates along the depth size
            output_depth = 0
            layer['layers'] = layer['layers'].split(',')

            for i in range(len(layer['layers'])):

                if int(layer['layers'][i]) < 0:
                    layer['layers'][i] = layer_index + int(layer['layers'][i])

                else:
                    layer['layers'][i] = int(layer['layers'][i])

                output_depth += shapes[layer['layers'][i]][2]

            shapes.append([shapes[layer['layers'][0]][0], shapes[layer['layers'][0]][1], output_depth])

        elif layer['type'] == 'shortcut':

            # Shortcut sums up the layers, preserving shape
            layer['from'] = layer_index + int(layer['from'])

            # I use last layer (layer_index) as referrence since it will have the same shape as the shortcut layer
            shapes.append([shapes[layer_index-1][0], shapes[layer_index-1][1], shapes[layer_index-1][2]])

        elif layer['type'] == 'yolo':

            layer['scale'] = int(layer['scale'])
            layer['num'] = int(layer['num'])
            layer['jitter'] = float(layer['jitter'])
            layer['ignore_thresh'] = float(layer['ignore_thresh'])
            layer['truth_thresh'] = float(layer['truth_thresh'])
            layer['random'] = float(layer['random'])
            shapes.append([CONFIG.DETECTION_RESOLUTION[layer['scale']],
                           CONFIG.DETECTION_RESOLUTION[layer['scale']],
                           len(CONFIG.DETECTION_RESOLUTION) * (np.shape(CONFIG.CLASSES)[0] + 5)])

    return layers, shapes


def get_annotations_paths():

    """
    Gets the path of all the files in the data folder

    Returns:
    annotation_files_paths -- list of file paths in the data folder
    """

    annotation_files_paths = [f for f in listdir(CONFIG.ANNOATIONS_PATH) if isfile(join(CONFIG.ANNOATIONS_PATH, f))]

    return annotation_files_paths


def load_annotations(files_paths):

    """
    Loads annotations for each image in the dataset

    Arguments:
    files_paths -- paths for all the image files in the dataset

    Returns:
    images -- annotations for all of the images in the dataset
    """

    images = []

    for index in range(len(files_paths)):

        tree = ET.parse(join(CONFIG.ANNOATIONS_PATH, files_paths[index]))
        root = tree.getroot()

        image = {}
        image['filename'] = str(root.find('filename').text)
        image['size'] = {}
        image['size']['width'] = int(root.find('size').find('width').text)
        image['size']['height'] = int(root.find('size').find('height').text)
        image['size']['depth'] = int(root.find('size').find('depth').text)

        image['boxes'] = []

        for object in root.iter('object'):

            box = {}
            box['name'] = object.find('name').text
            box['pose'] = object.find('pose').text
            box['xmin'] = int(object.find('bndbox').find('xmin').text)
            box['ymin'] = int(object.find('bndbox').find('ymin').text)
            box['xmax'] = int(object.find('bndbox').find('xmax').text)
            box['ymax'] = int(object.find('bndbox').find('ymax').text)

            image['boxes'].append(box)

        images.append(image)

    return images


def adapt_annotations(images):

    """

    Adapts anotations in order to ease reading and processing of images

    Arguments:
    images -- annotations for all of the images in the dataset

    Returns:
    images -- adapted information about the image annotations

    """

    for image_index, image in enumerate(images):

        if image['size']['width'] > image['size']['height']:
            image['size']['scale'] = CONFIG.WIDTH / image['size']['width']
            image['size']['resized_width'] = int(image['size']['scale'] * image['size']['width'])
            image['size']['resized_height'] = int(image['size']['scale'] * image['size']['height'])
            image['size']['x_padding'] = 0
            image['size']['y_padding'] = np.floor((CONFIG.WIDTH -
                                                   (image['size']['height'] * image['size']['scale'])) / 2)
        else:
            image['size']['scale'] = CONFIG.HEIGHT / image['size']['height']
            image['size']['resized_width'] = int(image['size']['scale'] * image['size']['width'])
            image['size']['resized_height'] = int(image['size']['scale'] * image['size']['height'])
            image['size']['x_padding'] = np.floor((CONFIG.HEIGHT -
                                                   (image['size']['width'] * image['size']['scale'])) / 2)
            image['size']['y_padding'] = 0

        for box_index, box in enumerate(image['boxes']):

            # Get class index
            box['class'] = CONFIG.CLASSES.index(box['name'])

            # Adapt cooordinates (in pixels) to new scale and padding
            box['xmin'] = box['xmin'] * image['size']['scale'] + image['size']['x_padding']
            box['ymin'] = box['ymin'] * image['size']['scale'] + image['size']['y_padding']
            box['xmax'] = box['xmax'] * image['size']['scale'] + image['size']['x_padding']
            box['ymax'] = box['ymax'] * image['size']['scale'] + image['size']['y_padding']
            box['center_x'] = np.floor((box['xmax'] + box['xmin']) / 2)
            box['center_y'] = np.floor((box['ymax'] + box['ymin']) / 2)
            box['width'] = box['xmax'] - box['xmin']
            box['height'] = box['ymax'] - box['ymin']

        image['boxes'], image['predictions'] = assign_anchors(image['boxes'])

        for box_index, box in enumerate(image['boxes']):

            # Determine the cell number where the box should be assigned
            box['x_cell'] = int(box['center_x'] // CONFIG.DETECTION_SIZES[box['stride_index']])
            box['y_cell'] = int(box['center_y'] // CONFIG.DETECTION_SIZES[box['stride_index']])

            # Determines the position of center of the box normalized to the cell size (bx and by from the YOLO paper)
            box['cell_norm_x'] = box['center_x'] / CONFIG.DETECTION_SIZES[box['stride_index']]
            box['cell_norm_y'] = box['center_y'] / CONFIG.DETECTION_SIZES[box['stride_index']]

            # Determines the position of center of the box normalized to the cell
            # coordinates (tx and ty from the YOLO paper)
            box['cell_norm_x'] = logit(box['cell_norm_x'] - box['x_cell'] + 0.0000001)
            box['cell_norm_y'] = logit(box['cell_norm_y'] - box['y_cell'] + 0.0000001)

            # Determine the width and height of the box normalized to the cell size (bw and bh from the YOLO paper)
            box['cell_norm_width'] = box['width'] / CONFIG.DETECTION_SIZES[box['stride_index']]
            box['cell_norm_height'] = box['height'] / CONFIG.DETECTION_SIZES[box['stride_index']]

            # Determine the width and height of the box (tw and th from the YOLO PAPER)
            box['cell_norm_width'] = np.log(box['cell_norm_width'] /
                                            (CONFIG.ANCHORS[box['stride_index']][box['anchor_index']][0] /
                                             CONFIG.DETECTION_SIZES[box['stride_index']]))
            box['cell_norm_height'] = np.log(box['cell_norm_height'] /
                                             (CONFIG.ANCHORS[box['stride_index']][box['anchor_index']][1] /
                                              CONFIG.DETECTION_SIZES[box['stride_index']]))

    return images


def assign_anchors(boxes):

    """

    Asigns each ground truth box to an anchor

    Arguments:
    boxes -- ground truth boxes

    Returns:
    boxes -- ground truth boxes
    predictions -- array holding prediction values associated with each ground truth bounding box

    """

    # Determine the IoUs betweeen each ground truth bounding box and each anchor box (from all resolutions)
    # shape [no_of_boxes, no_of_anchors]
    ious, stride_indexes, anchor_indexes = get_iou_from_annotations(boxes)

    # Determine the horizontal and vertical cell number for each ground truth box, in correlation with each of
    # the different resolutions
    x_cells, y_cells = get_cell_position(boxes, stride_indexes)

    # Sort the ious values
    order = np.argsort(ious, axis=1)

    # Apply sorting to all arrays
    ious = sort_array(ious, order)
    stride_indexes = sort_array(stride_indexes, order)
    anchor_indexes = sort_array(anchor_indexes, order)
    x_cells = sort_array(x_cells, order)
    y_cells = sort_array(y_cells, order)

    predictions = []
    predictions.append(np.zeros([CONFIG.DETECTION_RESOLUTION[0], CONFIG.DETECTION_RESOLUTION[0],
                                 np.shape(CONFIG.ANCHORS[0])[0]]))
    predictions.append(np.zeros([CONFIG.DETECTION_RESOLUTION[1], CONFIG.DETECTION_RESOLUTION[1],
                                 np.shape(CONFIG.ANCHORS[1])[0]]))
    predictions.append(np.zeros([CONFIG.DETECTION_RESOLUTION[2], CONFIG.DETECTION_RESOLUTION[2],
                                 np.shape(CONFIG.ANCHORS[2])[0]]))

    boxes_left_counter = np.shape(ious)[0]

    for anchor_index in reversed(range(0,np.shape(ious)[1])):

        iou_slice = ious[:, anchor_index]

        for box_counter in range(0, boxes_left_counter):

            # The index of the box with the highest IoU for that stride value
            max_box_index = int(np.argmax(iou_slice))

            # Check if the position is free
            if predictions[int(stride_indexes[max_box_index][anchor_index])]\
                    [int(y_cells[max_box_index][anchor_index])]\
                    [int(x_cells[max_box_index][anchor_index])]\
                    [int(anchor_indexes[max_box_index][anchor_index])] != 1:

                # Remove availability for that anchor box
                predictions[int(stride_indexes[max_box_index][anchor_index])]\
                    [int(y_cells[max_box_index][anchor_index])]\
                    [int(x_cells[max_box_index][anchor_index])]\
                    [int(anchor_indexes[max_box_index][anchor_index])] = 1

                # Loop over all the remaining IoUs of the selected box and distribute its IoUs in two categories,
                # <threshold which should predict 0 and will have prediction cost,
                # and those >threshold which will have no cost
                for iou_index in range (0, anchor_index):
                    if ious[max_box_index][iou_index] > CONFIG.OBJECTNESS_THRESHOLD:
                        predictions[int(stride_indexes[max_box_index][iou_index])][
                            int(y_cells[max_box_index][iou_index])][int(x_cells[max_box_index][iou_index])][
                            int(anchor_indexes[max_box_index][iou_index])] = 1  # ious[max_box_index][iou_index]

                # I remove this box from consideration, since I've just assigned it
                ious[max_box_index][:] = np.zeros((1, np.shape(ious)[1]))

                boxes[max_box_index]['stride_index'] = int(stride_indexes[max_box_index][anchor_index])
                boxes[max_box_index]['anchor_index'] = int(anchor_indexes[max_box_index][anchor_index])
                boxes_left_counter -= 1

            else:
                # If this is the case, it means that the anchor box with the best IoU was already taken by
                # another ground truth box, so I skip this oneby making it 0 and continue searching
                ious[max_box_index][anchor_index] = 0

    return boxes, predictions


def sort_array(input_array, sort_indexes):

    """

    Sorts an input array based on a list of indexes

    Arguments:
    input_array -- array to be sorted
    sort_indexes -- indexes used to sort

    Returns:
    output_arrat -- sorted array

    """

    output_array = np.empty(np.shape(input_array))

    for i in range(0, np.shape(output_array)[0]):
        for j in range(0, np.shape(output_array)[1]):
            output_array[i][j] = input_array[i][sort_indexes[i][j]]

    return output_array


def get_cell_position(boxes, stride_indexes):


    """

    Determines the cell to which a box should be assigned, for each resolution (stride)

    Arguments:
    boxes -- ground truth boxes
    stride_indexes -- index of resolution (stride)

    Returns:
    x_cells -- horizontal cell number for each box, accordingly to its associated resolution (stride)
    y_cells -- vertical cell number for each box, accordingly to its associated resolution (stride)

    """

    # Will store the x cell positions for each of the anchor box, shape [no of boxes, no of anchors boxes]
    x_cells = np.empty([len(boxes), np.shape(stride_indexes)[1]], dtype=int)
    y_cells = np.empty([len(boxes), np.shape(stride_indexes)[1]], dtype=int)

    for box_index, box in enumerate(boxes):

        for stride_index, stride in enumerate(stride_indexes[box_index]):

            # determine the cell number where the box should be assigned
            x_cells[box_index][stride_index] = int(box['center_x'] // CONFIG.DETECTION_SIZES[stride])
            y_cells[box_index][stride_index] = int(box['center_y'] // CONFIG.DETECTION_SIZES[stride])

    return x_cells, y_cells


def get_iou_from_annotations(boxes):

    """

    Creates arrays with IoUs values, resolution (stride) indexes, and anchor indexes, used when assigning ground
    truth box to specific anchor boxes while creating the labels

    Arguments:
    boxes -- ground truth boxes

    Returns:
    ious -- Intersection-over-Unions for each ground truth box
    stride_indexes -- resolution (stride) indexes for each ground truth box
    anchor_indexes -- anchor indexes for each ground truth box

    """

    # Compare the box with the set of anchors bot best fit when their centers are in the same position
    anchor_sizes = np.reshape(CONFIG.ANCHORS, [-1, 2])

    # Determine the coordinates of the top-let and botton-right corners (x1, y1, x2, y2) of all anchors, when
    # centered at (0,0)
    anchors = np.append(np.multiply(anchor_sizes, -1 / 2), np.multiply(anchor_sizes, -1), 1)

    # Will store IoUs of each pair (anchor box, ground truth box)
    ious = np.empty([len(boxes), len(anchors)])

    # Will store the anchor index of each corresponding IoU (relative to its stride)
    anchor_indexes = np.empty([len(boxes), len(anchors)], dtype=int)

    # Will store stride index of each corresponding IoU
    stride_indexes = np.empty([len(boxes), len(anchors)], dtype=int)

    for box_index, box in enumerate(boxes):

        # Deine the box size
        box_size = (box['width'], box['height'])

        # Determine the top-left and bottom-right coordinates of all boxes, when centered at (0,0)
        box = np.append(np.multiply(box_size, -1/2), np.multiply(box_size, 1/2), 0)

        # Will store coordinates of the intersection
        i = np.empty([len(anchors),4])

        # Will store intersection area values
        intersections = np.empty(len(anchors))

        # Will store union area values
        unions = np.empty(len(anchors))

        for anchor_index, anchor in enumerate(anchors):

            # Determine intersections coordinates

            # x1
            i[anchor_index][0] = np.maximum(anchor[0], box[0])

            # y1
            i[anchor_index][1] = np.maximum(anchor[1], box[1])

            # x2
            i[anchor_index][2] = np.maximum(anchor[2], box[2])

            # y2
            i[anchor_index][3] = np.maximum(anchor[3], box[3])

            # Calculate intersection area
            intersections[anchor_index] = np.maximum(i[anchor_index][3] - i[anchor_index][1], 0) * \
                                          np.maximum(i[anchor_index][2] - i[anchor_index][0], 0)

            # Determine unions areas
            unions[anchor_index] = np.multiply(anchor_sizes[anchor_index][0], anchor_sizes[anchor_index][1]) + \
                                   np.multiply(box_size[0], box_size[1]) - intersections[anchor_index]

            # Calculate the IoUs
            ious[box_index][anchor_index] = intersections[anchor_index] / unions[anchor_index]

            # Calculate the stride index
            stride_indexes[box_index][anchor_index] = anchor_index // np.shape(CONFIG.DETECTION_RESOLUTION)[0]

            # Calculates the anchor index relative to its stride
            anchor_indexes[box_index][anchor_index] = anchor_index % np.shape(CONFIG.DETECTION_RESOLUTION)[0]

    return ious, stride_indexes, anchor_indexes


def generate_labels(images):

    """

    Creates arrays with IoUs values, resolution (stride) indexes, and anchor indexes, used when assigning ground
    truth box to specific anchor boxes while creating the labels

    Arguments:
    images -- annotations for the images in the dataset

    Returns:
    y -- labels for the dataset

    """

    # Calculate the depth of a single anchor box set of labels
    depth = np.shape(CONFIG.CLASSES)[0]+5

    y = []

    for stride_index, stride in enumerate(CONFIG.DETECTION_RESOLUTION):

        y_temp = np.zeros([len(images), CONFIG.DETECTION_RESOLUTION[stride_index],
                           CONFIG.DETECTION_RESOLUTION[stride_index],
                           depth * np.shape(CONFIG.ANCHORS)[0]])

        for image_index in range(len(images)):

            image = images[image_index]

            for box_index, box in enumerate(image['boxes']):
                # Each anchor box will have as depth [tx, ty, tw, th, p0, c1, c2, ...]
                # Each cell will have as depth multiple anchors

                if box['stride_index'] == stride_index:

                    # Determine the starting position (i.e. 0 for anchor 0, 80 for anchor 1 ...)
                    starting_pos = int(box['anchor_index'] * depth)

                    # tx
                    y_temp[image_index][box['y_cell']][box['x_cell']][starting_pos] = box['cell_norm_x']

                    # ty
                    y_temp[image_index][box['y_cell']][box['x_cell']][starting_pos + 1] = box['cell_norm_y']

                    # tw
                    y_temp[image_index][box['y_cell']][box['x_cell']][starting_pos + 2] = box['cell_norm_width']

                    # th
                    y_temp[image_index][box['y_cell']][box['x_cell']][starting_pos + 3] = box['cell_norm_height']

                    # p0 = 1, since there is an object there
                    y_temp[image_index][box['y_cell']][box['x_cell']][starting_pos + 4] = 1

                    # Class
                    y_temp[image_index][box['y_cell']][box['x_cell']][starting_pos + box['class'] + 5] = 1

            for y_index in range(CONFIG.DETECTION_RESOLUTION[stride_index]):
                for x_index in range(CONFIG.DETECTION_RESOLUTION[stride_index]):
                    for anchor_index in range(np.shape(CONFIG.ANCHORS)[0]):
                        if image['predictions'][stride_index][y_index][x_index][anchor_index] != 1 and \
                                image['predictions'][stride_index][y_index][x_index][anchor_index]!= 0:
                            # Determine the starting position (i.e. 0 for anchor 0, 25 for anchor 1 ...)
                            starting_pos = int(anchor_index * depth)
                            y_temp[image_index][y_index][x_index][starting_pos + 4] = \
                                image['predictions'][stride_index][y_index][x_index][anchor_index]

        y_temp = label_transform(y_temp)

        y.append(y_temp)

    y = np.concatenate(y, axis=1)

    return y
