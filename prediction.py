
import tensorflow as tf
import matplotlib.pyplot as plt
from utils import *
from scipy.special import logit, expit

def draw_dections(image, boxes):

    """
    Draws bounding boxes over an image

    Arguments:
    image -- image over which the boxes will be drawn
    boxes -- floats in [0.0, 1.0] with shape [num_bounding_boxes, 4], each box has (y_min, x_min, y_max, x_max)

    Returns:
    image -- image with bounding boxes
    """

    with tf.Session() as sess:
        shape = np.shape(sess.run(boxes))[0]

    if shape > 0:
        image = tf.image.draw_bounding_boxes([image], [boxes])

    return image


def get_predictions(y):

    """
    Processes the output of the network in order to select the predictions

    Arguments:
    y -- output data

    Returns:
    probs -- probabilities associated with each box
    boxes -- selected boxes
    classes -- classes associated with each box
    """

    probs = y[:, 4]
    boxes = get_boxes_from_predictions(y)
    classes = get_classes_from_predictions(y)
    probs, boxes, classes = apply_objectness_threshold(probs, boxes, classes)
    probs, boxes, classes = non_max_suppression(probs, boxes, classes)

    return probs, boxes, classes


def non_max_suppression(probs, boxes, classes):

    """
    Processes the output of the network in order to select the predictions

    Arguments:
    y -- output data

    Returns:
    probs -- probabilities associated with boxes
    boxes -- selected boxes
    classes -- classes associated with boxes
    """

    output_probs = []
    output_boxes = []
    output_classes = []

    # Loop over all classes
    for class_index, class_value in enumerate(CONFIG.CLASSES):

        # Select from 'classes' indices of those entries that belong to the current class
        class_indices = (classes == class_index)

        # Checks if any of the boxes are associated to that class
        if(len(classes[class_indices])):
            temp_probs = tf.boolean_mask(probs, class_indices)
            temp_probs = tf.cast(temp_probs, tf.float32)

            temp_boxes = tf.boolean_mask(boxes, class_indices)
            temp_classes = tf.boolean_mask(classes, class_indices)

            # Performs non-max-suppression over all the boxes of that class
            nms_indices = tf.image.non_max_suppression(temp_boxes, temp_probs, CONFIG.MAX_PRED_BOXES,
                                                       CONFIG.IOU_THRESHOLD)

            output_probs.append(tf.gather(temp_probs, nms_indices))
            output_boxes.append(tf.gather(temp_boxes, nms_indices))
            output_classes.append(tf.gather(temp_classes, nms_indices))

    if len(output_probs) != 0:
        output_probs = tf.concat(output_probs, axis=0)

    if len(output_boxes) != 0:
        output_boxes = tf.concat(output_boxes, axis=0)

    if len(output_classes) != 0:
        output_classes = tf.concat(output_classes, axis=0)

    return output_probs, output_boxes, output_classes


def get_classes_from_predictions(y_labels):

    """
    Determines which class was the one predicted by an anchor box

    Arguments:
    y_labels -- labels of the dataset

    Returns:
    classes -- list of predicted classes
    """

    # Selects the class labels
    classes = y_labels[:, 5:]

    # Chooses the biggest one as the class prediction
    classes = np.argmax(classes, axis=-1)

    return classes


def apply_objectness_threshold(probs, boxes, classes):

    """
    Chooses those boxes with objectness score greater than the threshold

    Arguments:
    probs -- probabilities associated with boxes
    boxes -- selected boxes
    classes -- classes associated with boxes

    Returns:
    output_probs -- probabilities associated with boxes, greater than the threshold
    output_boxes -- selected boxes with probabilities greater than the threshold
    output_classes -- classes associated with boxes with probabilities greater than the threshold
    """

    # Selects those
    mask = (probs >= CONFIG.OBJECTNESS_THRESHOLD)

    print('Maximum objectness score:', np.amax(probs))
    print('Minimum objectness score:', np.amin(probs))

    output_probs = tf.boolean_mask(probs, mask)
    output_boxes = tf.boolean_mask(boxes, mask)

    # Output_classes = tf.boolean_mask(classes, mask)
    output_classes = classes[mask]

    return output_probs, output_boxes, output_classes


def get_boxes_from_predictions(y):

    """
    Extracts an array of boxes from the prediction, in the appropriate format

    Arguments:
    y -- output data

    Returns:
    boxes -- boxes array
    """

    start_index = 0
    end_index = 0

    resolution_values = np.empty([np.shape(y)[0]])
    resolution_indexes = np.empty([np.shape(y)[0]], dtype=int)
    no_of_boxes_values = np.empty([np.shape(y)[0]])
    cell_x_pos = np.empty([np.shape(y)[0]])
    cell_y_pos = np.empty([np.shape(y)[0]])
    anchor_values = np.transpose(np.arange(0, np.shape(y)[0]))

    for resolution_index, resolution_value in enumerate(CONFIG.DETECTION_RESOLUTION):
        end_index += np.square(resolution_value) * np.shape(CONFIG.ANCHORS)[0]
        resolution_values[start_index:end_index] = np.ones([end_index-start_index]) * resolution_value
        resolution_indexes[start_index:end_index] = np.ones([end_index-start_index]) * resolution_index

        no_of_boxes_values[start_index:end_index] = np.ones([end_index-start_index]) * start_index
        start_index += np.square(resolution_value) * np.shape(CONFIG.ANCHORS)[0]

    cell_x_pos[:] = np.transpose(np.arange(0, np.shape(y)[0]))
    cell_x_pos[:] = ((cell_x_pos[:]-no_of_boxes_values[:]) % (resolution_values*CONFIG.CHANNELS)) // CONFIG.CHANNELS

    cell_y_pos[:] = np.transpose(np.arange(0, np.shape(y)[0]))
    cell_y_pos[:] = (cell_y_pos[:]-no_of_boxes_values[:]) // (resolution_values * CONFIG.CHANNELS)

    anchor_values[:] = np.transpose(np.arange(0, np.shape(y)[0]))
    anchor_values[:] = anchor_values[:] % np.shape(CONFIG.ANCHORS[0])[0]

    boxes = get_box_from_label(y[:,:4], resolution_values, cell_x_pos, cell_y_pos, resolution_indexes, anchor_values)

    boxes = boxes.astype(np.float32)

    return boxes


def get_box_from_label(input_coords, resolution_values, cell_x_pos, cell_y_pos, resolution_indexes, anchor_values):

    """

    Calculates box coordinates with format (y_min, x_min, y_max, x_max)

    Arguments:
    input_coords -- boxes with the YOLO format (tx, ty, tw, th)
    resolution_values -- list with number of cells associated with each different resolution (stride)
    cell_x_pos -- horizontal cell number for each box, accordingly to its associated resolution (stride)
    cell_y_pos -- vertical cell number for each box, accordingly to its associated resolution (stride)
    resolution_indexes -- resolution(stride) indexes, associated with each of the boxes
    anchor_values -- anchor box index values associated with each of the boxes

    Returns:
    output_coords -- boxes with format (y_min, x_min, y_max, x_max)

    """

    temp_coords = np.empty(np.shape(input_coords))
    output_coords = np.empty(np.shape(input_coords))

    temp_coords[:,0] = expit(input_coords[:,0]) + cell_x_pos
    temp_coords[:,1] = expit(input_coords[:,1]) + cell_y_pos

    # The coordinates of the box center, normalized to the image
    temp_coords[:,0] = temp_coords[:,0] / resolution_values
    temp_coords[:,1] = temp_coords[:,1] / resolution_values

    # The values of the box dimensions, normalized to the image
    temp_coords[:,2] = (np.exp(input_coords[:,2]) * (np.array(CONFIG.ANCHORS)[resolution_indexes, anchor_values, 0] /
                                                     np.array(CONFIG.DETECTION_SIZES)[resolution_indexes])) / \
                       np.array(CONFIG.DETECTION_RESOLUTION)[resolution_indexes]
    temp_coords[:,3] = (np.exp(input_coords[:,3]) * (np.array(CONFIG.ANCHORS)[resolution_indexes, anchor_values, 1] /
                                                     np.array(CONFIG.DETECTION_SIZES)[resolution_indexes])) / \
                       np.array(CONFIG.DETECTION_RESOLUTION)[resolution_indexes]

    # The coordinates of the firs corner and those of the opposite one
    output_coords[:,0] = temp_coords[:,1] - temp_coords[:,3] / 2
    output_coords[:,1] = temp_coords[:,0] - temp_coords[:,2] / 2
    output_coords[:,2] = temp_coords[:,1] + temp_coords[:,3] / 2
    output_coords[:,3] = temp_coords[:,0] + temp_coords[:,2] / 2

    return output_coords


def show_image(image):

    """

    Displays the image with bounding boxes drawn over it

    Arguments:
    image -- the image to be displayed

    """

    with tf.Session() as sess:
        im = sess.run(image)
        plt.imshow(im[0])
        plt.show()
