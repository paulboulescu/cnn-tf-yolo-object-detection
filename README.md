# YOLOv3 Implementation with TensorFlow (work in progress)

## About
A TensorFlow implementation, from ground up, of YOLOv3 (You Only Look Once) - an object detection algorithm.

## Installation
* Clone the repository
```
$ git clone https://paulboulescu@bitbucket.org/paulboulescu/cnn-tf-yolo-object-detection.git
```

## Requirements
* TensorFlow

## Details
* VOC2007 annotation files should be placed inside `data/Annotations/` folder
* VOC2007 image files should be placed inside `data/JPEGImages/` folder
* Configuration data is placed inside `cfg/yolov3.cfg` file

## Implemented Features
* Loads configuration file
* Loads annotation files (Pascal VOC2007)
* Loads images into mini-batches
* Draws bounding boxes and displays images
* Performs training
* Performs predictions

## Features to be implemented
* Command-line interface
* Saving and loading weights parameters
* Video and real-time detection integration

## Known Issues
* A thorough training, over the entire data set, hasn't been performed yet. Training the network over a very small batch of images, all containing a single class object, failed to produce the expected over-fitting but accurate detection result, suggesting that an implementation problem prevents the network from performing as expected. This issue is under consideration and measures are being taken in order to fix the problem. 

## Resources
* [The PASCAL Visual Object Classes Challenge 2007](http://host.robots.ox.ac.uk/pascal/VOC/voc2007/)
* [YOLOv3 Configuration File](https://github.com/pjreddie/darknet/blob/master/cfg/yolov3.cfg)

## References
* [YOLOv3: An Incremental Improvement](https://pjreddie.com/media/files/papers/YOLOv3.pdf)
* [YOLO_v3_tutorial_from_scratch](https://github.com/ayooshkathuria/YOLO_v3_tutorial_from_scratch)